#!/usr/bin/env python3

import configparser
import sys
import socket
import wsgiref, wsgiref.simple_server

config_file_name = '/etc/whichasn.ini'

default_values = {'email_administrator': "",
                  'url_documentation': "",
                  'sockfile': ""}

SECTION = "WHICHASN"
config = configparser.ConfigParser(default_values)
try:
    config_file = open(config_file_name)
except IOError:
    print("Cannot open configuration file %s" % config_file_name, file=sys.stderr)
    sys.exit(1)
config.readfp(config_file)
if not config.has_section(SECTION):
    config.add_section(SECTION)
email_admin = config.get(SECTION, 'email_administrator')
url_doc = config.get(SECTION, 'url_documentation')
sockfile = config.get(SECTION, 'sockfile')

def send_response(start_response, status, output, type):
    response_headers = [('Content-type', type),
                        ('Content-Length', str(len(output))),
                        ('Allow', 'GET')]
    start_response(status, response_headers)
            
def application(environ, start_response):
    global sockfile
    path = environ['PATH_INFO']
    query = path[1:]
    qualifier = environ['QUERY_STRING']
    try:
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.connect(sockfile)
        data = query
        if qualifier is not None and qualifier != "":
            data += " %s" % qualifier
        sock.sendall(bytes(data + "\n", "UTF-8"))
        received = str(sock.recv(4096), "UTF-8")
    except Exception as e:
        output =  "Cannot open the socket \"%s\" or write to it: %s" % (sockfile, e)
        send_response(start_response, '500 Internal server error', output, 'text/plain')
        return [output.encode()]
    finally:
        sock.close()
    output = received
    send_response(start_response, '200 OK', output, 'text/plain')
    return [output.encode()]

if __name__ == '__main__':
    httpd = wsgiref.simple_server.make_server('localhost', 8999, application)
    httpd.serve_forever()
