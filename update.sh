#!/bin/sh

rm -f riswhoisdump.IP*
wget -q http://www.ris.ripe.net/dumps/riswhoisdump.IPv4.gz
wget -q http://www.ris.ripe.net/dumps/riswhoisdump.IPv6.gz
gunzip -f *.gz
./query.py reload
