# WhichASN

WhichASN is a simple daemon to map IP prefixes to the origin AS
number. It uses [RIS](https://www.ripe.net/analyse/internet-measurements/routing-information-service-ris/routing-information-service-ris) dumps.

## Usage

When the daemon runs on the local machine, you can use `query.py` to
query it:

```
% ./query.py 2a01:4f8:200:1302::42
2a01:4f8::/29 24940
```

Which means that the IP address `2a01:4f8:200:1302::42` is currently
announced as a prefix of length 29, by AS (Autonomous System) number 
24940.

If you want to know more about this IP address prefix, or about this
AS, you can use tools like whois:

```
% whois AS24940
...
aut-num:        AS24940
as-name:        HETZNER-AS
org:            ORG-HOA1-RIPE
...
role:           Hetzner Online GmbH - Contact Role
address:        Hetzner Online GmbH
address:        Industriestrasse 25
address:        D-91710 Gunzenhausen
address:        Germany
```

Instead of an IP address or prefix, you can use as arguments:

* help: provides a very short help
* reload: reloads the database (warning, it takes time, it is
  typically used by the periodic script `update.sh`)
* info: returns a summary of the state of the daemon:

```
% ./query.py info
OK: 819807 IPv4 prefixes, 80034 IPv6 prefixes, 67665 AS
```

To have also the less-specific prefixes, you add 'A' (for All) at the
end:

```
% ./query.py 2001:408:ff01::1  A              
2001:408:ff01::/48 14793
2001:408::/32 14793
```

A Web interface also exists, you can query it at
`https://bgp.bortzmeyer.org/IPADDRESS`. For instance, with curl:

```
% curl https://bgp.bortzmeyer.org/2001:678:c::1
2001:678:c::/48 2484 2486
```

(Here, you can see the prefix is announced by two AS.)

To have also the less-specific prefixes, add the query 'A' (for All):

```
% curl https://bgp.bortzmeyer.org/1.53.2.1\?a
1.53.2.0/24 18403
1.53.0.0/20 18403
1.53.0.0/18 18403
1.52.0.0/14 18403
```

The Web interface is not really useful, since you could use the [RIPE stat
API](https://stat.ripe.net/docs/data_api) as well. But this one is
simpler, and produces plain text instead of JSON. Anyway, it is here mostly for the
[Fediverse](https://en.wikipedia.org/wiki/Fediverse) interface, [documented here](https://framagit.org/bortzmeyer/fediverse-bgp-bot).

## Installation

To install the daemon, you need to:

1. Get the source
2. Configure it (parameters at the beginning of the source code)
3. Ensure you have pre-requisites: a Python 3 interpreter, and the
   [pytricia](https://github.com/jsommers/pytricia) Python module
   (which implements [Patricia tries](https://en.wikipedia.org/wiki/Radix_tree).)
4. Configure your operating system to starts the daemon at boot time
   (warning: it can easily eats one gigabyte of memory)
5. Configure cron (or a similar program) to update the BGP tables, for
   instance, every day
6. The first time, download the RIS dumps (see the `update.sh` script
   for an example), start the daemon and enjoy (see Usage).

For the periodic task, you may use the `update.sh`sample script. From
cron, it could be:

```
28 5 * * * (cd /local/src/whichasn; ./update.sh)
```

Don't run it too often, it can easily take tens of seconds, during
which the daemon won't serve requests.

The daemon uses regular logging. On a systemd machine, you can get
information with:

```
%  journalctl | grep WHICHASN
Nov 02 14:47:15 ayla python3[32685]: WHICHASN - DEBUG - 2019-11-02 13:47:15Z - Searching IPv4 "82.251.62.29/32"
Nov 02 14:48:03 ayla python3[32685]: WHICHASN - DEBUG - 2019-11-02 13:48:03Z - Searching IPv6 "2001:4b98:dc0:41:216:3eff:fe27:3d3f/128"
```

The Web interface uses
[WSGI](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface) so
you need to install a WSGI interface, for instance, in the case of
Apache on Debian, the package `libapache2-mod-wsgi`. You then need a
configuration file (`/etc/whichasn.ini` by default) which indicates
where to find the Unix socket of the daemon, e.g.:

```
[WHICHASN]
sockfile = /var/run/whichasn.socket
```

A possible configuration for Apache is then:

```
    WSGIScriptAlias / /var/www/bgp.bortzmeyer.org/scripts/whichasn.py
    WSGIDaemonProcess bgp-tls.bortzmeyer.org processes=4 threads=8 display-name=%{GROUP}
    WSGIProcessGroup bgp-tls.bortzmeyer.org
```

For the script `whichasn.py`, you can use `whichasn-wsgi.py`, which
should work out-of-the-box.

## Implementation

WhichASN daemon does not listen on the Internet. This is because it is
not specially robust and, above all, because it is strictly
sequential. A misbehaving client could hang it forever. So, the daemon
only listens on a local Unix socket. To query it from the outside, you
need its Web interface, or its fediverse interface.

Because it is strictly sequential, it has internally no locks: it
allows it to update the database without fearing that a client will
arrive at the same time, and get corrupted data.

We use [pytricia](https://github.com/jsommers/pytricia), an
implementation of [Patricia
tries](https://en.wikipedia.org/wiki/Radix_tree), because just relying
on [the standard ipaddress
module](https://docs.python.org/3.7/library/ipaddress.html) is too
slow for the size of the [DFZ](https://en.wikipedia.org/wiki/Default-free_zone).

## Licence

See the file LICENSE

## Author

Stéphane Bortzmeyer <stephane+framagit@bortzmeyer.org>

## See also

Its fediverse interface
<https://framagit.org/bortzmeyer/fediverse-bgp-bot>
