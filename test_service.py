#!/usr/bin/env python3

# Test by executing 'pytest'
# https://docs.pytest.org/

# https://requests.readthedocs.io/en/master/
import requests

import re

info_response = re.compile('^OK: [0-9]+ IPv4 prefixes, [0-9]+ IPv6 prefixes, [0-9]+ AS$')

def test_basic():
    r = requests.get('https://bgp.bortzmeyer.org/')
    assert  r.status_code == 200 and r.headers['content-type'] == 'text/plain' and 'It is not an IP prefix' in r.text 
    
def test_help():
    r = requests.get('https://bgp.bortzmeyer.org/help')
    assert  r.status_code == 200 and r.headers['content-type'] == 'text/plain' and 'Send just an IP prefix' in r.text 
    
def test_info():
    r = requests.get('https://bgp.bortzmeyer.org/info')
    assert  r.status_code == 200 and r.headers['content-type'] == 'text/plain' and info_response.search(r.text) is not None
    
def test_ipv4():
    r = requests.get('https://bgp.bortzmeyer.org/209.170.200.1')
    assert r.status_code == 200 and r.headers['content-type'] == 'text/plain' and r.text == '209.170.200.0/24 13649'

def test_ipv4_prefix():
    r = requests.get('https://bgp.bortzmeyer.org/209.170.200.0/27')
    assert r.status_code == 200 and r.headers['content-type'] == 'text/plain' and r.text == '209.170.200.0/24 13649'

def test_ipv4_several():
    r = requests.get('https://bgp.bortzmeyer.org/194.0.9.1')
    assert r.status_code == 200 and r.headers['content-type'] == 'text/plain' and r.text == '194.0.9.0/24 2484 2486'

def test_ipv4_general():
    r = requests.get('https://bgp.bortzmeyer.org/119.170.0.1?A')
    assert r.status_code == 200 and r.headers['content-type'] == 'text/plain' and r.text == '119.170.0.0/18 9824\n119.170.0.0/17 9824\n119.170.0.0/16 9824'
    
def test_ipv4_none():
    r = requests.get('https://bgp.bortzmeyer.org/10.1.1.1')
    assert r.status_code == 200 and r.headers['content-type'] == 'text/plain' and r.text == ''

def test_ipv6():
    r = requests.get('https://bgp.bortzmeyer.org/2a0b:47c4:2000::1')
    assert r.status_code == 200 and r.headers['content-type'] == 'text/plain' and r.text == '2a0b:47c4:2000::/36 204805'

def test_ipv6_prefix():
    r = requests.get('https://bgp.bortzmeyer.org/2a0b:47c4:2000::/64')
    assert r.status_code == 200 and r.headers['content-type'] == 'text/plain' and r.text == '2a0b:47c4:2000::/36 204805'

# TODO an IPv6 prefix announced by several AS?
#def test_ipv6_several():
#    r = requests.get('https://bgp.bortzmeyer.org/2a0b:47c4:2000::/64')
#    assert r.status_code == 200 and r.headers['content-type'] == 'text/plain' and r.text == '2a0b:47c4:2000::/36 204805'

def test_ipv6_general():
    r = requests.get('https://bgp.bortzmeyer.org/2a0b:47c4:2000::/64?A')
    assert r.status_code == 200 and r.headers['content-type'] == 'text/plain' and r.text == '2a0b:47c4:2000::/36 204805\n2a0b:47c4::/32 204805'

def test_ipv6_none():
    r = requests.get('https://bgp.bortzmeyer.org/fd3:bad:dcaf:1::1')
    assert r.status_code == 200 and r.headers['content-type'] == 'text/plain' and r.text == ''

def test_notexist():
    r = requests.get('https://bgp.bortzmeyer.org/doesnotexistatall')
    assert  r.status_code == 200 and 'It is not an IP prefix' in r.text 


    
