#!/usr/bin/env python3

SOCKFILE = '/home/stephane/run/whichasn.socket'

import socket
import sys

if len(sys.argv) != 2 and len(sys.argv) != 3:
    raise Exception("Usage: %s command-or-ip-prefix [qualifier]" % sys.argv[0])
data = sys.argv[1]
if len(sys.argv) == 3:
    qualifier = sys.argv[2]
    data += " %s" % qualifier
else:
    qualifier = None
sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

try:
    sock.connect(SOCKFILE)
    sock.sendall(bytes(data + "\n", "utf-8"))
    received = str(sock.recv(4096), "utf-8")
finally:
    sock.close()

if received != "Done":    
    print(received)

