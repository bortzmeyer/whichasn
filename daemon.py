#!/usr/bin/env python3

SOCKFILE = '/home/stephane/run/whichasn.socket'
# The dump files are obtained from http://www.ris.ripe.net/dumps/
FILENAME4='riswhoisdump.IPv4'
FILENAME6='riswhoisdump.IPv6'
MINPEERS = 9 # Minimum number of peers to accept an IP address

import socketserver
import os
import shutil
import ipaddress
import re
import logging
import logging.handlers
import time

# https://github.com/jsommers/pytricia
import pytricia

# We need two Patricia tries because of the IPv6 "bug" https://github.com/jsommers/pytricia/issues/29
pyt4 = None
pyt6 = None
prefixes = None

def reload():
    global pyt4
    global len_pyt4
    global pyt6
    global len_pyt6
    global prefixes
    global all_as
    global len_all_as

    comment = re.compile("^\s*(%|\s*$)") # Comment, or empty
    all_as = {}
    # Assumes a sequential server, and that nobody will access the tries during reloading.
    pyt4 = pytricia.PyTricia(32)
    pyt6 = pytricia.PyTricia(128) # Default is to use only 32 bits, which gives strange results for IPv6
    prefixes = {}
    for filename in [FILENAME4, FILENAME6]:
        input = open(filename)
        line_n = 0
        for line in input.readlines():
            line = line[:-1]
            line_n += 1
            if comment.search(line) is None:
                (origin, prefix, numpeers) = line.split("\t")
                numpeers = int(numpeers)
                if origin not in all_as:
                    all_as[origin] = True
                if numpeers >= MINPEERS:
                    if prefix.find(':') >= 0:
                        prefix = ipaddress.IPv6Network(prefix, strict=True)
                        pyt6.insert(prefix, prefix.prefixlen, None)
                    else:
                        prefix = ipaddress.IPv4Network(prefix, strict=True)
                        pyt4.insert(prefix, prefix.prefixlen, None)
                    if prefix not in prefixes:
                        prefixes[prefix] = [origin,]
                    else:
                        prefixes[prefix].append(origin)
        input.close()
    logger.info("Files have been read")
    # Length is may be long to compute, we do it just once
    len_pyt4 = len(pyt4)
    len_pyt6 = len(pyt6)
    len_all_as = len(all_as)
    logger.info("Reloading done, %i IPv4 prefixes, %i IPv6 prefixes, %i AS" %
          (len_pyt4, len_pyt6, len_all_as))

# To make the server process requests in parallel, we could use
# ThreadingMixIn.  But we don't since we expect requests to be short
# and always done by nice clients.  Do not expose this to the
# Internet.
class MyHandler(socketserver.StreamRequestHandler):

    def handle(self):
        allPrefixes = False
        global pyt4
        global len_pyt4
        global pyt6
        global len_pyt6
        global prefixes
        global all_as
        global len_all_as

        ws = re.compile("\s+")
        self.data = self.rfile.readline().strip().decode()
        logger.debug("New connection")
        try:
            # TODO handle the empty path
            if self.data == "help":
                self.wfile.write("Send just an IP prefix".encode())
                time.sleep(50)
                return
            elif self.data == "reload":
                reload()
                self.wfile.write("Done".encode())
                return
            elif self.data == "info": 
                resp = "OK: %i IPv4 prefixes, %i IPv6 prefixes, %i AS" % \
                    (len_pyt4, len_pyt6, len_all_as)
                logger.debug("/info: %s" % resp)
                self.wfile.write(resp.encode())
                return
            else: # OK, it can be an IP address, may be with the A qualifier at the end
                args = ws.split(self.data)
                if len(args) > 2:
                    self.wfile.write("Too many arguments".encode())
                    time.sleep(50)
                    return
                if len(args) > 1:
                    (addr, qualifier) = args
                    if qualifier.upper() != "A":
                        self.wfile.write(("Unknown qualifier \"%s\"" % qualifier).encode())
                        time.sleep(50)
                        return
                    allPrefixes = True
                else:
                    addr = args[0]
                if addr.find(':') >= 0:
                    try:
                        prefix = ipaddress.IPv6Network(addr, strict=False)
                        logger.debug("Searching IPv6 \"%s\"" % prefix)
                        gprefix = pyt6.get_key(prefix)
                    except ipaddress.AddressValueError:
                        logger.debug("Invalid message \"%s\"" % addr)
                        msg = "I don't understand \"%s\". It is not an IP prefix." % addr
                        self.wfile.write(msg.encode())
                        return
                else:
                    try:
                        prefix = ipaddress.IPv4Network(addr, strict=False)
                    except ipaddress.AddressValueError:
                        logger.debug("Invalid message \"%s\"" % addr)
                        msg = "I don't understand \"%s\". It is not an IP prefix." % addr
                        self.wfile.write(msg.encode())
                        return
                    logger.debug("Searching IPv4 \"%s\"" % prefix)
                    gprefix = pyt4.get_key(prefix)
                if gprefix is None:
                    self.wfile.write("".encode())
                else:
                    if gprefix.find(':') >= 0:
                        gprefix = ipaddress.IPv6Network(gprefix, strict=True)
                        pyt = pyt6
                    else:
                        gprefix = ipaddress.IPv4Network(gprefix, strict=True)
                        pyt = pyt4
                    resp = "%s %s" % (gprefix, " ".join(prefixes[gprefix]))
                    if allPrefixes:
                        more = True
                        while more:
                            pprefix = pyt.parent(gprefix)
                            if pprefix is None:
                                more = False
                            else:
                                if pprefix.find(':') >= 0:
                                    pprefix = ipaddress.IPv6Network(pprefix, strict=True)
                                else:
                                    pprefix = ipaddress.IPv4Network(pprefix, strict=True)
                                resp = "%s\n%s %s" % (resp, pprefix, " ".join(prefixes[pprefix]))
                                gprefix = pprefix
                    self.wfile.write(resp.encode())
        except BrokenPipeError: # client disappeared
            logger.debug("Cannot reply, broken pipe")
            
if __name__ == "__main__":
    try:
        os.unlink(SOCKFILE)
    except FileNotFoundError:
        pass
    server = socketserver.UnixStreamServer(SOCKFILE, MyHandler)
    os.chmod(SOCKFILE, 0o660)
    shutil.chown(SOCKFILE, group='whichasn')
    logger = logging.getLogger('whichasn')
    logger.setLevel(logging.DEBUG)
    ft = logging.Formatter(fmt = 'WHICHASN - %(levelname)s - %(asctime)s - %(message)s',
                                                  datefmt = '%Y-%m-%d %H:%M:%SZ')
    ft.converter = time.gmtime
    fh = logging.handlers.SysLogHandler(address="/dev/log", facility="local0")
    fh.setFormatter(ft)
    logger.addHandler(fh)
    reload()
    server.serve_forever()





                                                                                                                                                                                    
